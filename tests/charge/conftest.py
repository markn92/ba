import pytest
from evrouting.charge.T import SoCProfile, SoCFunction, ChargingFunction, Label
from itertools import count


@pytest.fixture
def soc_profile():
    cost = 1
    capacity = 4

    return cost, capacity, SoCProfile(cost, capacity)


@pytest.fixture
def soc_profile_2():
    cost = 2
    capacity = 4

    return cost, capacity, SoCProfile(cost, capacity)


@pytest.fixture
def ch_function():
    c = 2
    capacity = 4

    return c, capacity, ChargingFunction(c, capacity)


@pytest.fixture
def dummy_ch():
    c = 0
    capacity = 4
    return c, capacity, ChargingFunction(c, capacity, initial_soc=capacity)


@pytest.fixture
def ch_function_fast():
    c = 3
    capacity = 4

    return c, capacity, ChargingFunction(c, capacity)


last_cs_generator = count()


@pytest.fixture
def label(soc_profile_2):
    _, _, profile = soc_profile_2
    return Label(
        t_trip=10,
        soc_last_cs=2,
        soc_profile_cs_v=profile,
        last_cs=next(last_cs_generator)
    )


@pytest.fixture
def init_setup():
    capacity = 4
    last_cs = next(last_cs_generator)
    init_label = Label(
        t_trip=10,
        soc_last_cs=4,
        soc_profile_cs_v=SoCProfile(0, capacity),
        last_cs=last_cs
    )

    next_label = Label(
        t_trip=10 + 10,
        soc_last_cs=4,
        soc_profile_cs_v=SoCProfile(1, capacity),
        last_cs=last_cs
    )

    return last_cs, init_label, next_label


@pytest.fixture
def soc_function(label, ch_function):
    _, _, cf = ch_function
    return SoCFunction(label, cf)


@pytest.fixture
def soc_function_fast(label, ch_function_fast):
    _, _, cf = ch_function_fast
    return SoCFunction(label, cf)
