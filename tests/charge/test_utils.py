import pytest
from copy import copy
from evrouting.charge.utils import LabelPriorityQueue
from evrouting.charge.T import Label
from evrouting.charge.factories import SoCFunctionFactory, ChargingFunctionMap


@pytest.fixture
def q(label, ch_function):
    _, _, cf = ch_function
    dummy_cf = {label.last_cs: cf}
    q = LabelPriorityQueue(SoCFunctionFactory(dummy_cf), l_set=[])
    q.insert(label)

    # create min
    label = Label(
        t_trip=8,
        soc_last_cs=2,
        soc_profile_cs_v=label.soc_profile_cs_v,
        last_cs=1
    )

    dummy_cf[label.last_cs] = cf
    q.insert(label)

    yield q
    del q


class TestProrityQueue:
    def test_empty(self, q: LabelPriorityQueue):
        q.delete_min()
        q.delete_min()

        with pytest.raises(KeyError):
            q.delete_min()

    def test_peak(self, q: LabelPriorityQueue):
        assert q.peak_min().t_trip == 8

    def test_insert_same(self, q: LabelPriorityQueue, ch_function):
        """Should be no problem."""
        _, _, cf = ch_function
        label = q.peak_min()
        q.remove_item(label)
        q.insert(label)

    def test_dominance_check(self, label, soc_function, soc_function_fast):
        label_fast = Label(
            t_trip=label.t_trip,
            soc_last_cs=label.soc_last_cs,
            last_cs=2,
            soc_profile_cs_v=label.soc_profile_cs_v,
        )
        cf = {
            label.last_cs: soc_function.cf_cs,
            label_fast.last_cs: soc_function_fast.cf_cs
        }
        f_soc = SoCFunctionFactory(cf=cf)

        # minimum is dominated by lset
        l_set = [label, label_fast]
        queue = LabelPriorityQueue(f_soc=f_soc, l_set=l_set)
        queue.insert(label)

        assert not queue

        # minimum is not dominated by lset
        l_set = [label]
        queue = LabelPriorityQueue(f_soc=f_soc, l_set=l_set)
        queue.insert(label_fast)

        assert queue
