from math import inf

import pytest
from evrouting.charge.T import SoCProfile, ChargingFunction, SoCFunction


class TestSoCProfile:
    def test_profile_limit(self, soc_profile):
        cost, capacity, p = soc_profile
        assert p(capacity) == capacity - cost

    def test_profile_above_limit(self, soc_profile):
        cost, capacity, p = soc_profile
        assert p(capacity + 10) == capacity

    def test_profile_under_limit(self, soc_profile):
        cost, capacity, p = soc_profile
        assert p(cost - 1) == -inf

    def test_profile_lower_limit(self, soc_profile):
        cost, capacity, p = soc_profile
        assert p(cost) == 0

    def test_profile_within_limit(self, soc_profile):
        cost, capacity, p = soc_profile
        assert p(1.5) == 1.5 - cost

    def test_compound(self):
        cost_a = 1
        cost_b = 3
        capacity = 2

        p_a = SoCProfile(cost_a, capacity)
        p_b = SoCProfile(cost_b, capacity)

        p = p_a + p_b

        assert p.path_cost == cost_a + cost_b
        assert p.capacity == capacity


class TestSoCFunction:
    def test_minimum(self, soc_function):
        """Not necessary to charge so minimum is trip time."""
        assert soc_function.minimum == soc_function.t_trip

    def test_minimum_with_charge(self, soc_function):
        """Empty battery at charge station."""
        soc_function.soc_last_cs = 0

        # Needs to charge 2 Wh because path has costs
        # of 2. Charging takes additional 2 Wh / charging_coefficient
        # of time compared to the sole trip time.
        assert soc_function._calc_minimum() == \
               2 / soc_function.cf_cs.c + soc_function.t_trip

    def test_minimum_with_and_soc_charge(self, soc_function):
        """Empty battery at charge station."""
        soc_function.soc_last_cs = 1

        # Needs to charge 1 Wh because path has costs
        # of 2. Charging takes additional 1 Wh / charging_coefficient
        # of time compared to the sole trip time.
        assert soc_function._calc_minimum() == \
               1 / soc_function.cf_cs.c + soc_function.t_trip

    def test_below_t_trip(self, soc_function):
        """For t < t_trip, the current vertex is not feasible."""
        assert soc_function(9) == -inf

    def test_t_trip(self, soc_function):
        """
        Reaches the vertex without charging with the initial SoC 2 Wh
        reduced by th cost 2 Wh = 0 Wh.
        """
        assert soc_function(10) == 0

    def test_above_t_trip(self, soc_function):
        """Adds 1 h of charging to the case above."""
        assert soc_function(11) == 2

    def test_below_t_trip_need_to_fill(self, soc_function):
        """For t < t_trip, the current vertex is not feasible."""
        soc_function.soc_last_cs = 0
        assert soc_function(10) == -inf

    def test_t_trip_need_to_fill(self, soc_function):
        """Needs to charge to reach the current vertex."""
        soc_function.soc_last_cs = 0
        t_fill = 2 / soc_function.cf_cs.c
        assert soc_function(10 + t_fill) == 0.

    def test_above_t_trip_need_to_fill(self, soc_function):
        """Adds 1 h of charging."""
        soc_function.soc_last_cs = 0
        t_fill = 2 / soc_function.cf_cs.c
        assert soc_function(11 + t_fill) == 2

    def test_dummy_feasible(self, soc_function):
        """Cost < SoC at (dummy) Charging Station."""
        # Make dummy
        soc_function.cf_cs.c = 0

        assert soc_function.minimum == 10

    def test_dummy_not_feasible(self, soc_function):
        """Cost higher than soc at c."""
        # Make dummy
        soc_function.cf_cs.c = 0
        soc_function.soc_last_cs = 1

        assert soc_function._calc_minimum() == -inf

    def test_dominance(self, soc_function, soc_function_fast):
        assert soc_function_fast.dominates(soc_function)

    def test_dominance_circle(self, init_setup, dummy_ch):
        last_cs, init_label, next_label = init_setup
        _, _, cf = dummy_ch
        assert SoCFunction(init_label, cf).dominates(
            SoCFunction(next_label, cf)
        )


class TestChargingFunction:
    def test_creation(self, ch_function):
        c, capacity, cf = ch_function
        assert not cf.is_dummy

    def test_above_limit(self, ch_function):
        """Charge over capacity."""
        c, capacity, cf = ch_function

        # Above by time
        assert cf(3, 0) == capacity

        # Above by initial soc
        assert cf(1, 5) == capacity

        # Above by combination of time and initial soc
        assert cf(1, 3) == capacity

    def test_within_limit(self, ch_function):
        c, capacity, cf = ch_function

        assert cf(1) == 2
        assert cf(1, initial_soc=1) == 3

    def test_dummy_no_initial_soc_given(self):
        c = 0
        capacity = 4

        cf = ChargingFunction(c, capacity)

        assert cf.is_dummy

        with pytest.raises(ValueError):
            cf(3)

    def test_dummy(self):
        c = 0
        capacity = 4
        initial_soc = 3

        cf = ChargingFunction(c, capacity, initial_soc)

        assert cf(5) == initial_soc
        assert cf(0) == initial_soc
        assert cf(1) == initial_soc

    def test_inverse(self, ch_function):
        c, capacity, cf = ch_function

        assert cf.inverse(1) == 0.5
        assert cf.inverse(4) == 2

    def test_inverse_above_limit(self, ch_function):
        c, capacity, cf = ch_function

        assert cf.inverse(-1) == 0

        # Maximum time to charge is to load completely
        assert cf.inverse(capacity + 1) == capacity / c

    def test_inverse_dummy(self):
        c = 0
        capacity = 4
        initial_soc = 3

        cf = ChargingFunction(c, capacity, initial_soc)

        with pytest.raises(ValueError):
            cf.inverse(0)
