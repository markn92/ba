from .config import get_graph, edge_case


def test_edge_case():
    G = get_graph(edge_case)
    assert G.number_of_nodes() == 3
    assert G.number_of_edges() == 3
