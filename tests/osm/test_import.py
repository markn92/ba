from evrouting.osm.imports import slice_array, crop, Way


def test_slice():
    way = list(range(10))
    node_pass_count = {w: 1 for w in way}
    node_pass_count[0] = 2
    node_pass_count[4] = 2
    node_pass_count[6] = 2
    node_pass_count[9] = 2

    slices = slice_array(way, node_pass_count)

    assert slices == [[0, 1, 2, 3, 4], [4, 5, 6], [6, 7, 8, 9]]


def test_slice_short():
    way = [0, 1]
    node_pass_count = {w: 1 for w in way}
    slices = slice_array(way, node_pass_count)

    assert slices == [[0, 1]]


def test_crop():
    way = Way(id=0, nds=[0, 1, 2, 3, 4], tags=None)
    known_nodes = {1, 2, 4}

    ways = crop(way, known_nodes)
    assert [w.nds for w in ways] == [[1, 2]]

    way = Way(id=0, nds=[0, 1, 2, 3, 4], tags=None)
    known_nodes = {0, 2, 3, 4}

    ways = crop(way, known_nodes)
    assert [w.nds for w in ways] == [[2, 3, 4]]

    way = Way(id=0, nds=[0, 1, 2, 3, 4], tags=None)
    known_nodes = {0, 1, 3, 4}

    ways = crop(way, known_nodes)
    assert [w.nds for w in ways] == [[0, 1], [3, 4]]
