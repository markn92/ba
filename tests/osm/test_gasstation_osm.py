import networkx as nx

from evrouting import gasstation
from evrouting.T import Result
from evrouting.osm.profiles import car
from evrouting.osm.routing import shortest_path, GasstationAccessFunctions
from evrouting.graph_tools import DISTANCE_KEY


def test_charge_shortest_route_dimensions(map_graph):
    """Full tank is enough to get there. Must be equal to shortest path."""
    s = (51.75041438844966, 6.9332313537597665)
    t = (51.75657783347559, 7.000350952148438)
    _s = map_graph.find_nearest(s)
    _t = map_graph.find_nearest(t)

    consumption = 0.5  # kWh/s
    # Traveltime gonna be less than 10 min = 600 sek = 300 kWh.
    # Therefore initial soc of 300 000

    f = GasstationAccessFunctions(consumption)

    result = gasstation.shortest_path(G=map_graph,
                                      charging_stations=map_graph.charging_stations,
                                      s=_s,
                                      t=_t,
                                      initial_soc=300000,
                                      final_soc=0,
                                      capacity=300000,
                                      f=f,
                                      extended_graph=None,
                                      contracted_graph=None
                                      )

    path = shortest_path(map_graph, _s, _t, car)

    assert type(result) is Result
    assert result.charge_path == path.charge_path


def test_charge_shortest_route_stop(map_graph):
    """Full tank is enough to get there. Must be equal to shortest path."""
    s = (51.75041438844966, 6.9332313537597665)
    t = (51.75657783347559, 7.000350952148438)
    _s = map_graph.find_nearest(s)
    _t = map_graph.find_nearest(t)

    consumption = 0.5 * 1000  # Wh/s
    f = GasstationAccessFunctions(consumption)

    # Travel time to first charging station is < 5 min = 300 sek := 150 kWh
    # => Initial soc of 150 000 is enough to charge but not to reach target.
    initial_soc = 150000  # Wh
    result = gasstation.shortest_path(G=map_graph,
                                      charging_stations=map_graph.charging_stations,
                                      s=_s,
                                      t=_t,
                                      initial_soc=initial_soc,
                                      final_soc=0,
                                      capacity=300000,
                                      f=f,
                                      extended_graph=None,
                                      contracted_graph=None
                                      )
    assert type(result) is Result

    charge_nodes = [(n, t) for n, t in result.charge_path if t > 0]
    assert len(charge_nodes) == 1

    charge_node, charge_time = charge_nodes[0]
    charging_station_c = f._charging_coefficient(map_graph, charge_node)  # Wh/s
    # calc travel time to charge_node an
    s_to_c = consumption * nx.shortest_path_length(map_graph, _s, charge_node, weight=DISTANCE_KEY)
    c_to_t = consumption * nx.shortest_path_length(map_graph, charge_node, _t, weight=DISTANCE_KEY)

    used_energy = initial_soc + charge_time * charging_station_c
    path_cost = s_to_c + c_to_t

    assert int(path_cost) == int(used_energy)

    path_s_to_c = nx.shortest_path(map_graph, _s, charge_node, weight=DISTANCE_KEY)
    path_c_to_t = nx.shortest_path(map_graph, charge_node, _t, weight=DISTANCE_KEY)
    assert [n for n, t in result.charge_path] == path_s_to_c + path_c_to_t[1:]
