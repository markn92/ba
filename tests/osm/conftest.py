import os
import json

import pytest

from evrouting.osm.imports import OSMGraph, read_osm
from evrouting.osm.profiles import car


@pytest.fixture
def graph():
    G = OSMGraph()

    node_coordinates = [
        (51.7705832, 7.0002595),
        (51.7696529, 6.9568520)
    ]

    for n_id, coordinates in enumerate(node_coordinates):
        lat, lon = coordinates
        # Add two nodes, that exist in osm test map
        G.add_node(n_id, lat=lat, lon=lon)
        G.insert_into_rtree(n_id)

    yield G
    del G


@pytest.fixture
def map_graph():
    STATIC_DIR = os.path.join(os.path.dirname(__file__), 'static')
    G = read_osm(os.path.join(STATIC_DIR, 'map.osm'), car)
    with open(os.path.join(STATIC_DIR, 'charging_stations.json'), 'r') as f:
        charging_stations = json.load(f)
    G.insert_charging_stations(charging_stations)

    yield G
    del G
