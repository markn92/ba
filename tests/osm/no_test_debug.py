import os
import json
import datetime
import pickle
import pandas as pd
from time import perf_counter
import pytest

from evrouting.osm.imports import OSMGraph
from evrouting import charge
from evrouting.graph_tools import consumption_function_time_factory

STATIC_DIR = os.path.join(os.path.dirname(__file__), 'static')


@pytest.fixture
def large_graph():
    with open(os.path.join(STATIC_DIR, 'medium_nurnberg.pck'), 'rb') as f:
        G: OSMGraph = pickle.load(f)
    G.rebuild_rtree()
    return G


@pytest.fixture
def tasks():
    path = os.path.join(STATIC_DIR, 'classic.csv')
    df = pd.read_csv(path, dtype={'start_node': str, 'target_node': str})
    return [(row['start_node'], row['target_node']) for _, row in df.iterrows()]


@pytest.fixture
def charging_stations():
    with open(os.path.join(STATIC_DIR, 'charging_stations.json'), 'r') as f:
        charging_stations = json.load(f)
    return charging_stations


def test_debug(large_graph, charging_stations, tasks):
    """Full tank is enough to get there. Must be equal to shortest path."""
    G = large_graph
    G.insert_charging_stations(charging_stations)

    c = consumption_function_time_factory(0.5)
    mu_s = 40000
    mu_t = 0
    capacity = 40000

    total_tasks = len(tasks)
    for i, (s, t) in enumerate(tasks):
        now = datetime.datetime.now().strftime("%H:%M:%S")
        print(f'{now}: {i}/{total_tasks} ({(i/total_tasks)*100:.2f} %)')
        nodes_before = set(G.nodes)
        edges_before = set(G.edges)
        start = perf_counter()
        charge.routing.shortest_path(
            G=G,
            charging_stations=G.charging_stations,
            s=s,
            t=t,
            initial_soc=mu_s,
            final_soc=mu_t,
            capacity=capacity,
            c=c
        )
        runtime = perf_counter() - start

        assert nodes_before ^ set(G.nodes) == set()
        assert edges_before ^ set(G.edges) == set()
        assert runtime < 100
