import pytest
from evrouting.utils import PriorityQueue


@pytest.fixture
def q():
    q = PriorityQueue()
    q.insert('high', 3)
    q.insert('low', 2)

    yield q
    del q


class TestProrityQueue:
    def test_insert(self, q):
        assert q.delete_min() == 'low'
        assert q.delete_min() == 'high'

        with pytest.raises(KeyError):
            q.delete_min()

    def test_peak(self, q):
        assert q.peak_min() == 'low'
        assert q.peak_min() == 'low'  # does not get removed

    def test_update(self, q):
        q.insert('high', 1)
        assert q.delete_min() == 'high'
        assert q.delete_min() == 'low'

        with pytest.raises(KeyError):
            q.delete_min()
