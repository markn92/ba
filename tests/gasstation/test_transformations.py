import networkx as nx

import pytest
from evrouting.gasstation.routing import (
    contract_graph,
    get_possible_arriving_soc,
    state_graph,
    insert_final_node,
    insert_start_node,
    compose_result,
    shortest_path
)
from evrouting.graph_tools import (
    label,
    CONSUMPTION_KEY,
    DISTANCE_KEY,
    CHARGING_COEFFICIENT_KEY
)
from tests.config import gasstation, init_config, gasstation_complete


class TestContraction:
    def label_map(self, G: nx.Graph) -> dict:
        return {label(G, n): n for n in G.nodes}

    def test_contraction(self):
        conf: dict = init_config(gasstation)
        G: nx.Graph = conf['G']
        H: nx.Graph = contract_graph(G,
                                     conf['charging_stations'],
                                     conf['capacity'])

        # Check available gas stations
        assert set(H.nodes) == conf['charging_stations']

    @pytest.mark.parametrize('u,v,weight,value', [
        ('s', 'a', CONSUMPTION_KEY, 1),
        ('s', 'a', DISTANCE_KEY, 1),
        ('s', 'f', '', None),  # Not exist
        ('s', 'b', '', None),  # Not exist
        ('s', 'd', '', None),  # Not exist
        ('s', 'c', CONSUMPTION_KEY, 2),
        ('s', 'c', DISTANCE_KEY, 2),
        ('s', 'e', '', None),
    ])
    def test_contraction_edges(self, u, v, weight, value):
        """
        Test edges and values of its weights.
        :param u: Node
        :param v: Node
        :param weight: Weight key to check.
        :param value: Value the edge weight should have.
            If None, it tests if the egde does not exist.
        """
        conf: dict = init_config(gasstation)
        H: nx.Graph = contract_graph(conf['G'],
                                     conf['charging_stations'],
                                     conf['capacity'])

        label_map = self.label_map(H)
        try:
            edge = (label_map[u], label_map[v])
        except KeyError:
            # One of the edges is no an charging station
            if value is None:
                return True
            raise

        if value is None:
            assert edge not in H.edges
        else:
            assert H.edges[edge][weight] == value


class MinimalExamples:
    U = 4
    w_lower = 3
    w_greater = 5

    @staticmethod
    def d(w):
        return 2 * w

    @pytest.fixture
    def graph_w_below_U(self):
        G = nx.Graph()
        G.add_edge(0, 1, **{CONSUMPTION_KEY: self.w_lower, DISTANCE_KEY: self.d(self.w_lower)})
        yield G
        del G

    @pytest.fixture
    def graph_w_gt_U(self):
        G = nx.Graph()
        G.add_edge(0, 1, **{CONSUMPTION_KEY: self.w_greater, DISTANCE_KEY: self.d(self.w_greater)})
        yield G
        del G

    @pytest.fixture
    def graph_w_eq_U(self):
        G = nx.Graph()
        G.add_edge(0, 1, **{CONSUMPTION_KEY: self.U, DISTANCE_KEY: self.d(self.U)})
        yield G
        del G


class TestPossibleArrivingSoC(MinimalExamples):

    def test_unequal_charging_coeff_w_eq_U(self, graph_w_eq_U):
        G = graph_w_eq_U
        G.add_node(0, **{CHARGING_COEFFICIENT_KEY: 1})
        G.add_node(1, **{CHARGING_COEFFICIENT_KEY: 2})

        gv_lower = get_possible_arriving_soc(G, 0, self.U)
        assert gv_lower == [0]

        gv_higher = get_possible_arriving_soc(G, 1, self.U)
        assert gv_higher == [0]

    def test_equal_charging_coeff_w_lt_U(self, graph_w_below_U):
        G = graph_w_below_U
        G.add_node(0, **{CHARGING_COEFFICIENT_KEY: 1})
        G.add_node(1, **{CHARGING_COEFFICIENT_KEY: 1})

        gv_lower = get_possible_arriving_soc(G, 0, self.U)
        assert gv_lower == [0]

        gv_higher = get_possible_arriving_soc(G, 1, self.U)
        assert gv_higher == [0]

    def test_unequal_charging_coeff_w_lt_U(self, graph_w_below_U):
        G = graph_w_below_U
        G.add_node(0, **{CHARGING_COEFFICIENT_KEY: 1})
        G.add_node(1, **{CHARGING_COEFFICIENT_KEY: 2})

        gv_lower = get_possible_arriving_soc(G, 0, self.U)
        assert gv_lower == [0]

        gv_higher = get_possible_arriving_soc(G, 1, self.U)
        assert gv_higher == [0, self.U - self.w_lower]

    def test_equal_charging_coeff_w_eq_U(self, graph_w_eq_U):
        G = graph_w_eq_U
        G.add_node(0, **{CHARGING_COEFFICIENT_KEY: 1})
        G.add_node(1, **{CHARGING_COEFFICIENT_KEY: 1})

        gv_lower = get_possible_arriving_soc(G, 0, self.U)
        assert gv_lower == [0]

        gv_higher = get_possible_arriving_soc(G, 1, self.U)
        assert gv_higher == [0]

    @pytest.mark.parametrize('c_0,c_1', [(1, 2), (1, 1)])
    def test_equal_unreachable(self, graph_w_gt_U, c_0, c_1):
        G: nx.Graph = graph_w_gt_U
        G.add_node(0, **{CHARGING_COEFFICIENT_KEY: c_0})
        G.add_node(1, **{CHARGING_COEFFICIENT_KEY: c_1})

        gv_lower = get_possible_arriving_soc(G, 0, self.U)
        assert gv_lower == [0]

        gv_higher = get_possible_arriving_soc(G, 1, self.U)
        assert gv_higher == [0]


class TestStateGraph(MinimalExamples):
    def test_unequal_charging_coeff_w_eq_U(self, graph_w_eq_U):
        G = graph_w_eq_U
        c_0 = 1
        c_1 = 2
        G.add_node(0, **{CHARGING_COEFFICIENT_KEY: c_0})
        G.add_node(1, **{CHARGING_COEFFICIENT_KEY: c_1})

        H: nx.Graph = state_graph(G, self.U)
        assert set(H.nodes) == {(0, 0), (1, 0)}
        assert len(H.edges) == 2
        assert H.edges[(0, 0), (1, 0)]['weight'] == self.U * c_0 + self.d(self.U)
        assert H.edges[(1, 0), (0, 0)]['weight'] == self.U * c_1 + self.d(self.U)

    def test_unequal_charging_coeff_w_lt_U(self, graph_w_below_U):
        G = graph_w_below_U
        c_0 = 1
        c_1 = 2
        G.add_node(0, **{CHARGING_COEFFICIENT_KEY: c_0})
        G.add_node(1, **{CHARGING_COEFFICIENT_KEY: c_1})

        H: nx.Graph = state_graph(G, self.U)
        assert set(H.nodes) == {(0, 0), (1, 0), (1, 1)}
        assert len(H.edges) == 3
        assert H.edges[(0, 0), (1, 1)]['weight'] == self.U * c_0 + self.d(self.w_lower)
        assert H.edges[(1, 0), (0, 0)]['weight'] == self.w_lower * c_1 + self.d(self.w_lower)
        assert H.edges[(1, 1), (0, 0)]['weight'] == (self.w_lower - 1) * c_1 + self.d(self.w_lower)

    def test_equal_charging_coeff_w_lt_U(self, graph_w_below_U):
        G = graph_w_below_U
        c_0 = 1
        c_1 = 1
        G.add_node(0, **{CHARGING_COEFFICIENT_KEY: c_0})
        G.add_node(1, **{CHARGING_COEFFICIENT_KEY: c_1})

        H: nx.Graph = state_graph(G, self.U)
        assert set(H.nodes) == {(0, 0), (1, 0)}
        assert len(H.edges) == 2
        assert H.edges[(0, 0), (1, 0)]['weight'] == self.w_lower * c_0 + self.d(self.w_lower)
        assert H.edges[(1, 0), (0, 0)]['weight'] == self.w_lower * c_1 + self.d(self.w_lower)

    def test_equal_charging_coeff_w_eq_U(self, graph_w_eq_U):
        G = graph_w_eq_U
        c_0 = 1
        c_1 = 1
        G.add_node(0, **{CHARGING_COEFFICIENT_KEY: c_0})
        G.add_node(1, **{CHARGING_COEFFICIENT_KEY: c_1})

        H: nx.Graph = state_graph(G, self.U)
        assert set(H.nodes) == {(0, 0), (1, 0)}
        assert len(H.edges) == 2
        assert H.edges[(0, 0), (1, 0)]['weight'] == self.U * c_0 + self.d(self.U)
        assert H.edges[(1, 0), (0, 0)]['weight'] == self.U * c_1 + self.d(self.U)

    @pytest.mark.parametrize('c_0,c_1', [(1, 2), (1, 1)])
    def test_equal_unreachable(self, graph_w_gt_U, c_0, c_1):
        G: nx.Graph = graph_w_gt_U
        G.add_node(0, **{CHARGING_COEFFICIENT_KEY: c_0})
        G.add_node(1, **{CHARGING_COEFFICIENT_KEY: c_1})

        H: nx.Graph = state_graph(G, self.U)
        assert len(H.nodes) == 2
        assert len(H.edges) == 0


class Integration:
    @pytest.fixture
    def graph_config(self):
        return init_config(gasstation_complete)

    @pytest.fixture
    def graph(self, graph_config):
        return graph_config['G']

    @pytest.fixture
    def contracted_graph(self, graph_config):
        return contract_graph(
            G=graph_config['G'],
            charging_stations=graph_config['charging_stations'],
            capacity=graph_config['capacity']
        )

    @pytest.fixture
    def extended_graph(self, contracted_graph, graph_config):
        return state_graph(
            G=contracted_graph,
            capacity=graph_config['capacity']
        )

    @pytest.fixture
    def inserted_s(self, extended_graph, contracted_graph, graph_config):
        return insert_start_node(
            s=0,
            graph_core=graph_config['G'],
            graph_contracted=contracted_graph,
            gas_stations=graph_config['charging_stations'],
            graph_extended=extended_graph,
            capacity=graph_config['capacity'],
            initial_soc=4
        )

    @pytest.fixture
    def inserted_t(self, inserted_s, contracted_graph, graph_config):
        return insert_final_node(
            t=3,
            graph_core=graph_config['G'],
            gas_stations=graph_config['charging_stations'],
            graph_extended=inserted_s,
            capacity=graph_config['capacity'],
            final_soc=0
        )


class TestIntegration(Integration):

    def test_contracted_graph(self, contracted_graph):
        G = contracted_graph
        assert len(G.nodes) == 2
        assert len(G.edges) == 1

        assert G.edges[1, 2][CONSUMPTION_KEY] == 3
        assert G.edges[1, 2][DISTANCE_KEY] == 6

    @pytest.mark.parametrize('u, v, weight', [
        ((1, 0), (2, 1), 4 * 1 + 6),
        ((2, 0), (1, 0), 3 * 2 + 6),
        ((2, 1), (1, 0), 2 * 2 + 6),
    ])
    def test_extended_graph(self, extended_graph, graph, u, v, weight):
        assert len(extended_graph.nodes) == 3
        assert len(extended_graph.edges) == 3
        assert extended_graph.edges[u, v]['weight'] == weight

    @pytest.mark.parametrize('u, v, weight', [
        ((1, 0), (2, 1), 4 * 1 + 6),
        ((2, 0), (1, 0), 3 * 2 + 6),
        ((2, 1), (1, 0), 2 * 2 + 6),
        ((0, 4), (2, 3), 2),
        ((0, 4), (1, 1), 6),
        ((1, 1), (2, 1), (4 - 1) * 1 + 6)
    ])
    def test_s_insert(self, inserted_s, u, v, weight):
        assert set(inserted_s.nodes) == {(0, 4), (2, 3), (1, 1), (2, 1), (1, 0), (2, 0)}
        assert len(inserted_s.edges) == 6
        assert inserted_s.edges[u, v]['weight'] == weight

    @pytest.mark.parametrize('u, v, weight', [
        ((1, 0), (2, 1), 4 * 1 + 6),
        ((2, 0), (1, 0), 3 * 2 + 6),
        ((2, 1), (1, 0), 2 * 2 + 6),
        ((0, 4), (2, 3), 2),
        ((0, 4), (1, 1), 6),
        ((1, 1), (2, 1), (4 - 1) * 1 + 6),
        ((2, 3), (3, 0), 1 * 2 + 8),
        ((1, 1), (3, 0), 1 * 1 + 4),
        ((2, 1), (3, 0), 3 * 2 + 8),
        ((1, 0), (3, 0), 2 * 1 + 4),
        ((2, 0), (3, 0), 4 * 2 + 8),
    ])
    def test_t_insert(self, inserted_t, u, v, weight):
        assert set(inserted_t.nodes) == {(0, 4), (2, 3), (1, 1), (2, 1), (1, 0), (2, 0), (3, 0)}
        assert len(inserted_t.edges) == 11
        assert inserted_t.edges[u, v]['weight'] == weight

    def test_shortest_path_reset_state_graph(self, graph, graph_config, extended_graph, contracted_graph):
        before = len(extended_graph.nodes)
        shortest_path(G=graph,
                      charging_stations=graph_config['charging_stations'],
                      s=graph_config['s'],
                      t=graph_config['t'],
                      initial_soc=graph_config['initial_soc'],
                      final_soc=graph_config['final_soc'],
                      capacity=graph_config['capacity'],
                      extended_graph=extended_graph,
                      contracted_graph=contracted_graph
                      )

        assert before == len(extended_graph.nodes)


class TestResult(Integration):
    def test_compose_result(self, graph, inserted_t):
        path = [(0, 4), (1, 1), (3, 0)]
        result = compose_result(graph, inserted_t, path)
        assert result.trip_time == 6 + 1 + 4
        assert result.charge_path == [(0, 0), (1, 1), (3, 0)]
