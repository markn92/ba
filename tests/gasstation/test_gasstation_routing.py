import networkx as nx
from evrouting.gasstation.routing import (
    shortest_path,
)
from evrouting.graph_tools import (
    DISTANCE_KEY, CONSUMPTION_KEY
)
from evrouting.T import EmptyResult
from tests.config import gasstation_complete, init_config


def test_shortest_path():
    conf = init_config(gasstation_complete)

    r = shortest_path(
        G=conf['G'],
        charging_stations=conf['charging_stations'],
        s=conf['s'],
        t=conf['t'],
        initial_soc=conf['initial_soc'],
        final_soc=conf['final_soc'],
        capacity=conf['capacity']
    )
    assert r.charge_path == [(0, 0), (1, 1), (3, 0)]
    assert r.trip_time == 11


def test_unreachable_initial_soc():
    conf = init_config(gasstation_complete)
    conf['initial_soc'] = 0

    r = shortest_path(
        G=conf['G'],
        charging_stations=conf['charging_stations'],
        s=conf['s'],
        t=conf['t'],
        initial_soc=conf['initial_soc'],
        final_soc=conf['final_soc'],
        capacity=conf['capacity']
    )
    assert isinstance(r, EmptyResult)


def test_unreachable():
    conf = init_config(gasstation_complete)
    G: nx.Graph = conf['G']

    G.add_edge(2, 3, **{
        DISTANCE_KEY: 10,
        CONSUMPTION_KEY: 5
    })
    G.add_edge(1, 3, **{
        DISTANCE_KEY: 10,
        CONSUMPTION_KEY: 5
    })

    r = shortest_path(
        G=conf['G'],
        charging_stations=conf['charging_stations'],
        s=conf['s'],
        t=conf['t'],
        initial_soc=conf['initial_soc'],
        final_soc=conf['final_soc'],
        capacity=conf['capacity']
    )
    assert isinstance(r, EmptyResult)


def test_unreachable_final_soc():
    conf = init_config(gasstation_complete)
    conf['final_soc'] = 4

    r = shortest_path(
        G=conf['G'],
        charging_stations=conf['charging_stations'],
        s=conf['s'],
        t=conf['t'],
        initial_soc=conf['initial_soc'],
        final_soc=conf['final_soc'],
        capacity=conf['capacity']
    )

    assert isinstance(r, EmptyResult)


def test_s_from_t_reachable():
    conf = init_config(gasstation_complete)
    conf['t'] = 2
    r = shortest_path(
        G=conf['G'],
        charging_stations=conf['charging_stations'],
        s=conf['s'],
        t=conf['t'],
        initial_soc=conf['initial_soc'],
        final_soc=conf['final_soc'],
        capacity=conf['capacity']
    )
    assert r.charge_path == [(0, 0), (2, 0)]
    assert r.trip_time == 2


def test_s_gasstation():
    conf = init_config(gasstation_complete)
    conf['s'] = 2
    conf['initial_soc'] = 0
    r = shortest_path(
        G=conf['G'],
        charging_stations=conf['charging_stations'],
        s=conf['s'],
        t=conf['t'],
        initial_soc=conf['initial_soc'],
        final_soc=conf['final_soc'],
        capacity=conf['capacity']
    )

    assert r.charge_path == [(2, 8), (3, 0)]
    assert r.trip_time == 8 + 8


def test_t_gasstation():
    conf = init_config(gasstation_complete)
    conf['t'] = 3

    r = shortest_path(
        G=conf['G'],
        charging_stations=conf['charging_stations'],
        s=conf['s'],
        t=conf['t'],
        initial_soc=conf['initial_soc'],
        final_soc=conf['final_soc'],
        capacity=conf['capacity']
    )
    assert r.charge_path == [(0, 0), (1, 1), (3, 0)]
    assert r.trip_time == 11
