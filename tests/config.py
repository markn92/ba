import networkx as nx
from typing import Set

from evrouting.T import Node, ChargingCoefficient
from evrouting.graph_tools import (
    node_convert, edge_convert, TemplateEdge, TemplateNode, charging_cofficient
)

# List of configs
config_list = [
    'edge_case',
    'edge_case_start_node_no_cs',
    'edge_case_a_slow',
    'gasstation'
]

edge_case = {
    'beta_s': 0,
    'beta_t': 0,
    'U': 4,
    's': 0,
    't': 2,
    'nodes': [
        TemplateNode('s', charging_coeff=1),
        TemplateNode('a', charging_coeff=2),
        TemplateNode('t'),
    ],
    'edges': [
        TemplateEdge(0, 1, distance=1, consumption=1),
        TemplateEdge(0, 2, distance=1, consumption=4),
        TemplateEdge(1, 2, distance=1, consumption=1),
    ]
}

gasstation = {
    'beta_s': 0,
    'beta_t': 0,
    'U': 2,
    's': 0,
    't': 2,
    'nodes': [
        TemplateNode('s', charging_coeff=1),
        TemplateNode('a', charging_coeff=1),  # <U
        TemplateNode('f', charging_coeff=1),  # >U
        TemplateNode('b'),
        TemplateNode('c', charging_coeff=1),  # <U via non cs
        TemplateNode('d'),
        TemplateNode('e', charging_coeff=1),  # >U via non cs
    ],
    'edges': [
        TemplateEdge(0, 1, distance=1, consumption=1),
        TemplateEdge(0, 2, distance=1, consumption=3),
        TemplateEdge(0, 3, distance=1, consumption=1),
        TemplateEdge(3, 4, distance=1, consumption=1),
        TemplateEdge(0, 5, distance=1, consumption=1),
        TemplateEdge(5, 6, distance=1, consumption=2),
    ]
}

edge_case_a_slow = {
    'beta_s': 0,
    'beta_t': 0,
    'U': 4,
    's': 0,
    't': 2,
    'nodes': [
        TemplateNode('s', charging_coeff=2),
        TemplateNode('a', charging_coeff=1),
        TemplateNode('t'),
    ],
    'edges': [
        TemplateEdge(0, 1, distance=1, consumption=1),
        TemplateEdge(0, 2, distance=1.5, consumption=4),
        TemplateEdge(1, 2, distance=1, consumption=1),
    ]
}

edge_case_start_node_no_cs = {
    'beta_s': 0,
    'beta_t': 0,
    'U': 4,
    's': 0,
    't': 2,
    'nodes': [
        TemplateNode('s'),
        TemplateNode('a', charging_coeff=2),
        TemplateNode('t'),
    ],
    'edges': [
        TemplateEdge(0, 1, distance=1, consumption=1),
        TemplateEdge(0, 2, distance=1, consumption=4),
        TemplateEdge(1, 2, distance=1, consumption=1),
    ]
}

no_charging = {
    'beta_s': 4,
    'beta_t': 0,
    'U': 4,
    's': 0,
    't': 2,
    'nodes': [
        TemplateNode('s'),
        TemplateNode('a'),
        TemplateNode('t'),
    ],
    'edges': [
        TemplateEdge(0, 1, distance=1, consumption=1),
        TemplateEdge(0, 2, distance=4, consumption=1),
        TemplateEdge(1, 2, distance=1, consumption=1),
    ]
}

gasstation_complete = {
    'beta_s': 4,
    'beta_t': 0,
    'U': 4,
    's': 0,
    't': 3,
    'nodes': [
        TemplateNode(),
        TemplateNode(charging_coeff=1),
        TemplateNode(charging_coeff=2),
        TemplateNode(),
    ],
    'edges': [
        TemplateEdge(0, 1, distance=6, consumption=3),
        TemplateEdge(0, 2, distance=2, consumption=1),
        TemplateEdge(1, 2, distance=6, consumption=3),
        TemplateEdge(1, 3, distance=4, consumption=2),
        TemplateEdge(2, 3, distance=8, consumption=4),
    ]
}


def get_graph(config: dict) -> nx.Graph:
    G = nx.Graph()

    for node_id, node in enumerate(config['nodes']):
        G.add_node(node_id, **node_convert(node))

    for edge in config['edges']:
        G.add_edge(edge.u, edge.v, **edge_convert(edge))

    return G


def get_charging_stations(config: dict) -> Set[Node]:
    return {
        idx for idx, n in enumerate(config['nodes'])
        if n.charging_coeff is not None
    }


def init_config(config: dict) -> dict:
    G = nx.Graph()
    S = set()

    for node_id, node in enumerate(config['nodes']):
        G.add_node(node_id, **node_convert(node))
        c: ChargingCoefficient = charging_cofficient(G, node_id)
        if c is not None:
            S.add(node_id)

    for edge in config['edges']:
        G.add_edge(edge.u, edge.v, **edge_convert(edge))

    return {
        'G': G,
        'charging_stations': S,
        's': config['s'],
        't': config['t'],
        'initial_soc': config['beta_s'],
        'final_soc': config['beta_t'],
        'capacity': config['U']
    }
