import sys
import threading, queue
from queue import Empty
from pyqtgraph.Qt import QtGui, QtCore
import pyqtgraph as pg

TERMINATE = '<DONE>'


def plotter(queue):
    # QtGui.QApplication.setGraphicsSystem('raster')
    app = QtGui.QApplication([])
    # mw = QtGui.QMainWindow()
    # mw.resize(800,800)

    win = pg.GraphicsWindow(title="Basic plotting examples")
    win.resize(1000, 600)
    win.setWindowTitle('pyqtgraph example: Plotting')

    # Enable antialiasing for prettier plots
    pg.setConfigOptions(antialias=True)

    p = win.addPlot(title="Updating plot")

    def update():
        data = queue.get()
        if data == TERMINATE:
            sys.exit(0)

        if len(data) == 1:
            x, y = data[0]
            p.plot([x], [y], pen=(200, 200, 200), symbolBrush=(255, 0, 0), symbolPen='w')
        else:
            x1, y1 = data[0]
            x2, y2 = data[1]
            p.plot([x1, x2], [y1, y2], pen=(200, 200, 200))

    timer = QtCore.QTimer()
    timer.timeout.connect(update)
    timer.start(10)

    QtGui.QApplication.instance().exec_()


def init():
    q = queue.Queue(100)
    threading.Thread(target=plotter, args=(q,), daemon=True).start()
    return q


if __name__ == '__main__':
    """
    q = Queue(1)
    Process(target=plotter, args=(q,)).start()
    for data in [[(1, 2)], [(3, 4), (5, 6)]]:
        q.put(data)
        time.sleep(2)
    time.sleep(2)
    """
