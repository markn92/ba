import json
import logging.config
import pickle
import os
from time import perf_counter
from evrouting.osm.imports import OSMGraph
from evrouting import charge
from evrouting.graph_tools import consumption_function_distance_factory

logger = logging.getLogger(__name__)


def get_graph():
    logger.info('Getting graph..')
    with open('oberpfalz-latest.pck', 'rb') as f:
        G: OSMGraph = pickle.load(f)
    G.rebuild_rtree()
    return G


def get_charging_stations():
    logger.info('Getting charging stations..')
    with open('charging_stations.json', 'r') as f:
        charging_stations = json.load(f)
    return charging_stations


def get_data(G, r):
    if type(r) == tuple:
        try:
            return [(G.nodes[n]['lon'], G.nodes[n]['lat']) for n in r if n is not None]
        except KeyError:
            logger.warning(r)
            return r
    else:
        return r


def log_to_file(logger: logging.Logger, filename):
    handler = logging.FileHandler(filename, mode='w')
    handler.setFormatter(logging.Formatter('%(message)s'))
    logger.handlers = [handler]


def main():
    G = get_graph()
    G.insert_charging_stations(get_charging_stations())

    c = consumption_function_distance_factory(0.5)
    mu_s = 40000
    mu_t = 0
    capacity = 40000
    tasks = [
        ('324801228', '4058518276'),
        ('325698766', '306460225')
    ]
    detail_logger = logging.getLogger('stats_detail')
    timestamp_logger = logging.getLogger('stats_timestamp')

    for detail_logger_level, timestamp_logger_level in [
        (logging.INFO, logging.DEBUG),
        (logging.DEBUG, logging.INFO)
    ]:
        detail_logger.setLevel(detail_logger_level)
        timestamp_logger.setLevel(timestamp_logger_level)

        for i, (s, t) in enumerate(tasks):
            if detail_logger.isEnabledFor(logging.DEBUG):
                filename = f'stats-{s}{t}.csv'
                logger.info(f'Filling file {filename}')
                log_to_file(detail_logger, filename)
            if timestamp_logger.isEnabledFor(logging.DEBUG):
                filename = f'timestamps-{s}{t}.csv'
                logger.info(f'Filling file {filename}')
                log_to_file(timestamp_logger, filename)

            logger.info(f'Solving {s} to {t}..')
            start = perf_counter()
            result = charge.routing.shortest_path(
                G=G,
                charging_stations=G.charging_stations,
                s=s,
                t=t,
                initial_soc=mu_s,
                final_soc=mu_t,
                capacity=capacity,
                c=c
            )
            runtime = perf_counter() - start
            logger.info(f'Solved in {runtime:.2f} s')
            logger.info(result.trip_time)
            try:
                os.rename('stats.csv', f'stats{i}.csv')
            except FileExistsError:
                os.remove(f'stats{i}.csv')
                os.rename('stats.csv', f'stats{i}.csv')
            except FileNotFoundError:
                pass


def get_logging_config():
    return {
        'version': 1,
        'formatters': {
            'main': {
                'class': 'logging.Formatter',
                'format': '%(asctime)s %(levelname)s %(message)s',
                'datefmt': '%d-%m-%Y %H:%M:%S',
            }
        },
        'handlers': {
            'console': {
                'class': 'logging.StreamHandler',
                'formatter': 'main'
            }
        },
        'loggers': {
            'stats_detail': {
                'level': 'DEBUG',
                'propagate': 0,
                'handlers': []
            },
            'stats_timestamp': {
                'level': 'DEBUG',
                'propagate': 0,
                'handlers': []
            },
            __name__: {
                'level': 'INFO',
                'handlers': ['console']
            }
        }
    }


if __name__ == '__main__':
    logging.config.dictConfig(get_logging_config())
    main()
