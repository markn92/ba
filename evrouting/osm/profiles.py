"""Defines Routing Profiles"""


def speed(way, profile):
    profile_speed = min(
        profile['speeds'][way.tags['highway']], profile['maxspeed']
    )

    try:
        max_speed = float(way.tags['maxspeed'])
    except (KeyError, ValueError):
        speed = profile_speed
    else:
        speed = min(profile_speed, max_speed)

    return speed


car = {
    "maxspeed": 140,
    "speeds": {
        "motorway": 130,
        "motorway_link": 60,
        "trunk": 100,
        "trunk_link": 60,
        "primary": 50,
        "primary_link": 50,
        "secondary": 50,
        "secondary_link": 50,
        "tertiary": 50,
        "tertiary_link": 50,
        "unclassified": 30,
        "residential": 30,
        "living_street": 3,
        "service": 3
    },
    "highway_whitelist": {
        'motorway',
        'motorway_link',
        'trunk',
        'trunk_link',
        'primary',
        'primary_link',
        'secondary',
        'secondary_link',
        'tertiary',
        'tertiary_link',
        'residential',
        'living_street',
        'unclassified',
        'service'
    },
    "access": ["access", "vehicle", "motor_vehicle", "motorcar"]
}
