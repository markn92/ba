"""
Convert a Open Street Maps `.map` format file into a networkx directional graph.

This parser is based on the osm to networkx tool
from Loïc Messal (github : Tofull)

Added :
- python3.6 compatibility
- networkx v2 compatibility
- cache to avoid downloading the same osm tiles again and again
- distance computation to estimate length of each ways (useful to compute the shortest path)

"""
import xml.sax
import logging
import itertools
import random
from time import perf_counter
from collections import namedtuple, defaultdict
from typing import List

import networkx as nx
import rtree

from evrouting.graph_tools import CHARGING_COEFFICIENT_KEY, DISTANCE_KEY, HAVERSINE_KEY
from evrouting.osm.const import ms_to_kmh
from evrouting.osm.profiles import speed, car
from evrouting.osm.routing import point, haversine_distance

logger = logging.getLogger(__name__)


def get_charging_stations_nodes(G, charging_stations) -> List:
    nodes = []
    for s in charging_stations:
        n = G.find_nearest((s['lat'], s['lon']), distance_limit=500)
        if n is not None:
            nodes.append(n)
    return nodes


class OSMGraph(nx.DiGraph):
    """
    Adding some functionality to the graph for convenience when
    working with actual geo data from osm, such as a spatial index and
    a method to get an entry node by the spacial index for some coordinates.

    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.charging_stations = set()

        # Data structures for spatial index.
        # Rtree only supports indexing by integers, but the
        # node ids are strings. Therefore a mapping is introduced.
        self._rtree = rtree.index.Index()
        self._int_index = itertools.count()
        self._int_index_map = {}

    def insert_into_rtree(self, node):
        """Insert node into rtree."""
        info = self.nodes[node]
        lat, lon = info['lat'], info['lon']
        index = next(self._int_index)
        self._rtree.insert(index, (lon, lat, lon, lat))
        self._int_index_map[index] = node

    def rebuild_rtree(self):
        """Rebuild tree. Necessary because it cannot be pickled."""
        self._rtree = rtree.index.Index()
        self._int_index = itertools.count()
        self._int_index_map = {}
        for n in self.nodes:
            self.insert_into_rtree(n)

    def _clean_charging_stations(self):
        # Clean data
        for n in self.charging_stations:
            del self.nodes[n][CHARGING_COEFFICIENT_KEY]

    def insert_charging_stations(self, charging_stations, num=None):
        """Insert Charging Stations"""
        self._clean_charging_stations()

        # Find all __unique__ matching nodes to charging stations
        candidates = {}
        for s in charging_stations:
            n = self.find_nearest((s['lat'], s['lon']), distance_limit=500)
            if n is not None and n not in candidates:
                candidates[n] = s

        if num is not None:
            try:
                n_candidates = random.sample(candidates.keys(), num)
            except ValueError:
                logger.warning(f'Could not draw {num} stations from '
                               f'total {len(candidates)} stations.')
            else:
                candidates = {n: candidates[n] for n in n_candidates}

        self.charging_stations = {
            self._make_node_to_charging_station(n, s) for n, s in candidates.items()
        }

    def _make_node_to_charging_station(self, n, cs_data):
        self.nodes[n][CHARGING_COEFFICIENT_KEY] = cs_data['power'] / 3.6  # in Wh/s
        return n

    def find_nearest(self, v: point, distance_limit=None):
        """
        Find nearest point to location v within radius
        of distance_limit.
        """
        lat_v, lon_v = v

        index_n = next(self._rtree.nearest(
            (lon_v, lat_v, lon_v, lat_v), 1
        ))
        n = self._int_index_map[index_n]

        if distance_limit is not None:
            d = haversine_distance(
                self.nodes[n]['lat'],
                self.nodes[n]['lon'],
                lat_v,
                lon_v,
                unit_m=True
            )
            if d > distance_limit:
                n = None

        return n


def read_osm(osm_xml_data,
             profile=car,
             charging_stations_nodes: List = None,
             contract=False
             ) -> OSMGraph:
    """
    Read graph in OSM format from file specified by name or by stream object.
    Create Graph containing all highways as edges.

    """
    osm = OSM(osm_xml_data, keep_nodes=charging_stations_nodes)
    G = OSMGraph()

    # Add ways
    for w in osm.ways.values():
        if 'highway' not in w.tags:
            continue
        if w.tags['highway'] not in profile['highway_whitelist']:
            continue

        if contract:
            d = 0
            for u_id, v_id in zip(w.nds[:-1], w.nds[1:]):
                u, v = osm.nodes[u_id], osm.nodes[v_id]
                # Travel-time from u to v
                d += haversine_distance(u.lon, u.lat, v.lon, v.lat, unit_m=True)  # in m
            t = d / (speed(w, profile) / ms_to_kmh)  # in s
            _add_egde(G, w.nds[0], w.nds[-1], w, d, t)
        else:
            for u_id, v_id in zip(w.nds[:-1], w.nds[1:]):
                u, v = osm.nodes[u_id], osm.nodes[v_id]
                # Travel-time from u to v
                d = haversine_distance(u.lon, u.lat, v.lon, v.lat, unit_m=True)  # in m
                t = d / (speed(w, profile) / ms_to_kmh)  # in s
                _add_egde(G, u_id, v_id, w, d, t)

    # Complete the used nodes' information
    for n_id, data in G.nodes(data=True):
        n = osm.nodes[n_id]
        data['lat'] = n.lat
        data['lon'] = n.lon
        data['id'] = n.id
        G.insert_into_rtree(n_id)

    return G


def _add_egde(G, u_id, v_id, w, d, t):
    if w.tags.get('oneway', 'no') == 'yes':
        # ONLY ONE DIRECTION
        G.add_edge(u_id, v_id, **{
            DISTANCE_KEY: t,
            HAVERSINE_KEY: d
        })
    else:
        # BOTH DIRECTION
        G.add_edge(u_id, v_id, **{
            DISTANCE_KEY: t,
            HAVERSINE_KEY: d
        })
        G.add_edge(v_id, u_id, **{
            DISTANCE_KEY: t,
            HAVERSINE_KEY: d
        })


Node = namedtuple('Node', ['id', 'lon', 'lat', 'tags'])
Way = namedtuple('Way', ['id', 'nds', 'tags'])


def slice_array(waypoints, node_pass_count):
    slices = []
    end_last_slice = 0
    for i in range(1, len(waypoints) - 1):
        if node_pass_count[waypoints[i]] > 1:
            slices.append(waypoints[end_last_slice: i + 1])
            end_last_slice = i
    slices.append(waypoints[end_last_slice:])
    return slices


def split(way, node_pass_count):
    """
    Slice way at every crossing i.e. when a waypoint is passend by
    multiple ways.
    """

    # slice the node-array using this nifty recursive function
    slices = slice_array(way.nds, node_pass_count)

    # create a way object for each node-array slice
    return [Way(id=way.id + "-%d" % i, nds=s, tags=way.tags) for i, s in enumerate(slices)]


def filter_ways(ways: dict, known_nodes: dict, kind='hard'):
    if kind == 'hard':
        return hard_cuts(ways, known_nodes)
    elif kind == 'split':
        return split_cuts(ways, known_nodes)


def hard_cuts(ways: dict, known_nodes: dict):
    return {
        way_id: way
        for way_id, way in ways.items() if all(n in known_nodes for n in way.nds)
    }


def split_cuts(ways: dict, known_nodes: dict):
    def crop_unknown(way: Way, known_nodes):
        ways = []
        cur_nds = []
        for n in way.nds:
            if n in known_nodes:
                cur_nds.append(n)
            else:
                if len(cur_nds) > 1:
                    ways.append(
                        Way(id=f'{way.id}-{len(ways)}', nds=cur_nds, tags=way.tags)
                    )
                cur_nds = []

        if len(cur_nds) > 1:
            ways.append(
                Way(id=f'{way.id}-{len(ways)}', nds=cur_nds, tags=way.tags)
            )

        return ways

    return {
        w.id: w
        for way in ways.values()
        for w in crop_unknown(way, known_nodes)
    }


class OSM(object):
    def __init__(self, osm_xml_data, keep_nodes: List = None):
        """ File can be either a filename or stream/file object.

        set `is_xml_string=False` if osm_xml_data is a filename or a file stream.
        """
        nodes = {}
        ways = {}
        node_histogram = defaultdict(lambda: 0)
        keep_nodes = keep_nodes if keep_nodes is not None else []
        for n in keep_nodes:
            node_histogram[n] += 1

        class OSMHandler(xml.sax.ContentHandler):
            def __init__(self):
                super().__init__()
                self.currElem = None

            @classmethod
            def setDocumentLocator(self, loc):
                pass

            @classmethod
            def startDocument(self):
                pass

            @classmethod
            def endDocument(self):
                pass

            @classmethod
            def startElement(self, name, attrs):
                if name == 'node':
                    self.currElem = Node(attrs['id'], float(attrs['lon']), float(attrs['lat']), tags={})
                elif name == 'way':
                    self.currElem = Way(attrs['id'], [], {})
                elif name == 'tag':
                    self.currElem.tags[attrs['k']] = attrs['v']
                elif name == 'nd':
                    node_ref = attrs['ref']
                    node_histogram[node_ref] += 1
                    self.currElem.nds.append(node_ref)

            @classmethod
            def endElement(self, name):
                if name == 'node':
                    nodes[self.currElem.id] = self.currElem
                elif name == 'way' and len(self.currElem.nds) > 1:
                    ways[self.currElem.id] = self.currElem

            @classmethod
            def characters(self, chars):
                pass

        xml.sax.parse(osm_xml_data, OSMHandler)

        self.nodes = nodes

        # use that histogram to split all ways, replacing the member set of ways
        self.ways = {
            way.id: way for split_ways in [
                split(w, node_histogram) for w in ways.values()
            ] for way in split_ways
        }
        logger.debug('Filtering Nodes..')
        s = perf_counter()
        self.ways = filter_ways(self.ways, self.nodes, 'hard')
        logger.debug(f'Took {perf_counter() - s:.2f} s')
