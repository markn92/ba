from typing import Tuple
from math import radians, cos, sin, asin, sqrt

import networkx as nx

from evrouting.T import Result, EmptyResult
from evrouting.graph_tools import (
    DISTANCE_KEY, sum_weights, AccessFunctions
)
from evrouting.osm.const import ms_to_kmh
from evrouting.osm.profiles import car

lat = float
lon = float
point = Tuple[lat, lon]


def haversine_distance(lon1, lat1, lon2, lat2, unit_m=True):
    """
    Calculate the great circle distance between two points
    on the earth (specified in decimal degrees)
    default unit : km
    """
    # convert decimal degrees to radians
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])

    # haversine formula
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
    c = 2 * asin(sqrt(a))
    r = 6371  # Radius of the Earth in kilometers. Use 3956 for miles
    if unit_m:
        r *= 1000
    return c * r


def a_start_heuristic(G, profile):
    def dist(u, v):
        return haversine_distance(
            G.nodes[u]['lat'],
            G.nodes[u]['lon'],
            G.nodes[v]['lat'],
            G.nodes[v]['lon'],
            unit_m=True
        ) / profile['maxspeed'] * ms_to_kmh

    return dist


def shortest_path(G, s: point, t: point, profile) -> Result:
    """Calc A* shortest path."""

    try:
        path = nx.astar_path(G, s, t, heuristic=a_start_heuristic(G, profile))
    except nx.NetworkXNoPath:
        return EmptyResult()

    return Result(
        trip_time=sum_weights(G, path, DISTANCE_KEY),
        charge_path=[(n, 0) for n in path]
    )


def get_coordinates(G, n):
    lon = G.nodes[n]['lon']
    lat = G.nodes[n]['lat']
    return lon, lat


def get_coord_string(G, n) -> str:
    """:returns: lon, lat of n"""
    try:
        return "{},{}".format(*get_coordinates(G, n))
    except KeyError:
        return ""


def to_coordinates(G, path):
    """
    Path of nodes to path of coordinates.

    Note: Coordinates are (lon, lat) to conform to
        geojson.
    """

    return list(map(get_coordinates, path))


class GasstationAccessFunctions(AccessFunctions):
    def __init__(self, consumption_coefficient):
        super().__init__()
        self.c = consumption_coefficient

    def consumption(self, G, u, v):
        return self.c * self._distance(G, u, v)

    def charging_coefficient(self, G, v):
        return 1 / self._charging_coefficient(G, v)

    def path_consumption(self, G, path):
        return self.c * self.path_distance(G, path)

    @staticmethod
    def shortest_path(G, u, v):
        return nx.astar_path(G, u, v, heuristic=a_start_heuristic(G, profile=car))
