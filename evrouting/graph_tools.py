from collections import namedtuple

import networkx as nx
from evrouting.T import (
    Wh,
    ChargingCoefficient,
    Time,
    Node,
    NodeData,
    EdgeData,
    ConsumptionFunction
)

TemplateEdge = namedtuple('Edge', ['u', 'v', 'distance', 'consumption'])
TemplateNode = namedtuple(
    'Node', ['label', 'charging_coeff'], defaults=(None, None)
)

LABEL_KEY = 'label'
CHARGING_COEFFICIENT_KEY = 'c'
DISTANCE_KEY = 'weight'
CONSUMPTION_KEY = 'c'
HAVERSINE_KEY = 'haversine'


def node_convert(n: TemplateNode) -> NodeData:
    return {LABEL_KEY: n.label, CHARGING_COEFFICIENT_KEY: n.charging_coeff}


def edge_convert(e: TemplateEdge) -> EdgeData:
    return {DISTANCE_KEY: e.distance, CONSUMPTION_KEY: e.consumption}


def consumption(G: nx.Graph, u: Node, v: Node) -> Wh:
    return G.edges[u, v][CONSUMPTION_KEY]


def distance(G: nx.Graph, u: Node, v: Node) -> Time:
    return G.edges[u, v][DISTANCE_KEY]


def charging_cofficient(G: nx.Graph, n: Node) -> ChargingCoefficient:
    return G.nodes[n][CHARGING_COEFFICIENT_KEY]


def label(G: nx.Graph, u: Node) -> str:
    return G.nodes[u][LABEL_KEY]


def sum_weights(G, path, weight: str) -> float:
    """
    :param weight: either key so weights are G[u][v][weight] or
        a function f(G, u, v) -> float.
    """
    return sum(G[u][v][weight] for u, v in zip(path[:-1], path[1:]))


def consumption_function_distance_factory(consumption: float) -> ConsumptionFunction:
    """
    :param consumption: in kWh/m
    """

    def c(G, u, v):
        """Returns consumption in Wh from u to v."""
        try:
            return G[u][v][HAVERSINE_KEY] * consumption
        except KeyError:
            return G[u][v][CONSUMPTION_KEY]

    return c


def consumption_function_time_factory(consumption: float) -> ConsumptionFunction:
    """
    :param consumption: in kWh/s
    """

    def c(G, u, v):
        """Returns consumption in Wh from u to v."""
        try:
            return G[u][v][DISTANCE_KEY] * consumption
        except KeyError:
            return G[u][v][CONSUMPTION_KEY]

    return c


class AccessFunctions:
    """
    Class for dependency injections to access path
    attributes in flexible ways.
    """

    def __init__(self,
                 get_distance=None,
                 get_consumption=None,
                 get_charging_coefficient=None
                 ):
        self._distance = get_distance or distance
        self._consumption = get_consumption or consumption
        self._charging_coefficient = get_charging_coefficient or charging_cofficient

    def distance(self, G, u, v):
        return self._distance(G, u, v)

    def consumption(self, G, u, v):
        return self._consumption(G, u, v)

    def charging_coefficient(self, G, v):
        return self._charging_coefficient(G, v)

    def path_distance(self, G, path):
        """:return: Distance of path."""
        return sum_weights(G, path, weight=DISTANCE_KEY)

    def path_consumption(self, G, path):
        """:returns: consumption of given path."""
        return sum_weights(G, path, weight=CONSUMPTION_KEY)

    @staticmethod
    def shortest_path(G, u, v):
        return nx.algorithms.shortest_path(G, u, v, weight=DISTANCE_KEY)
