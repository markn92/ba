import networkx as nx
from dataclasses import dataclass
from typing import Tuple, Union, NewType, Dict, Any, List, Callable

Node = int
Edge = Tuple[Node, Node]

NodeData = Dict[str, Any]
EdgeData = Dict[str, Any]

Wh = NewType('Wh', Union[float, int])
SoC = NewType('SoC', Wh)

ChargingCoefficient = Union[float, int, None]

Time = Union[float, int]

ConsumptionFunction = Callable[[nx.Graph, Node, Node], float]


@dataclass
class Result:
    trip_time: Union[Time, None]
    charge_path: List[Tuple[Node, Time]]


class EmptyResult(Result):
    def __init__(self):
        super().__init__(trip_time=None, charge_path=[])
