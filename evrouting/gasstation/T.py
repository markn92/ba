from typing import Tuple, TypeVar, Callable, List

import networkx as nx
from evrouting.T import Node, SoC

State = Tuple[Node, SoC]

N = TypeVar('N')

DistFunction = Callable[[nx.Graph, N, N, str], List[N]]
