# Routing for Electric Vehicles
## Tests
Teste are done using PyTest. To run the tests, the 
dev dependencies must be installed:

```shell script
pipenv install --dev
```

Then simply run pytest within the root folder:

```shell script
pipenv run pytest
```

## Extras

Create charging stations used of test region:
 
```shell script
python data.py --cut 51.7806 6.8067 51.7570 6.8741 Ladesaeulenkarte_Datenbankauszug30_04_2020.csv tests/osm/static/charging_stations_raesfeld.json
```

### Draw Graphs used for Testing

To draw all graphs, respectively create ```.tex``` snippets (that make use of the 
[tikz-network](https://www.ctan.org/pkg/tikz-network)), use:

```shell script
python draw_graphs.py [path]

    path -  Path to the location where the .tex files
            should be exported to. Default is './plots'.
```
