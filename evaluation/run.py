import argparse
import pickle
import gc
import datetime
import json
import logging
from logging.config import dictConfig
from time import perf_counter
from pathlib import Path

import yaml

from evrouting.osm.imports import read_osm, get_charging_stations_nodes
from evaluation.lib.config import (
    InitConf, RankConf, QueryConf, QueryCycle, AnyConf, apply_conversions
)
from evaluation.lib.benchmarks import init, rank, query, query_cycles

logger = logging.getLogger(__name__)

conf_map = {
    'init': InitConf,
    'rank': RankConf,
    'query': QueryConf,
    'query_cycle': QueryCycle
}

benchmarks_map = {
    'init': init,
    'rank': rank,
    'query': query,
    'query_cycle': query_cycles
}

# Directories
base = Path(__file__).parent
static_dir = base.joinpath('static')
maps_dir = static_dir.joinpath('maps')
maps_cache_dir = static_dir.joinpath('mapcache')


def get_map(map_name: str, charging_stations):
    logger.info('Importing map {}'.format(map_name))
    osm_path = maps_dir.joinpath(map_name)
    cache_path = maps_cache_dir.joinpath(osm_path.with_suffix('.pck').name)
    try:
        with open(cache_path, 'rb') as f:
            graph = pickle.load(f)
        logger.info('Loaded map from cache {}'.format(cache_path))
    except FileNotFoundError:
        start = perf_counter()
        graph = read_osm(str(osm_path))
        runtime = perf_counter() - start
        logger.info(f'Importing map to raw graph '
                    f'({len(graph.nodes)} Nodes, {len(graph.edges)} Edges)'
                    f' took {runtime:.2f} s'
                    )
        start = perf_counter()
        cs_nodes = get_charging_stations_nodes(graph, charging_stations)
        runtime = perf_counter() - start
        logger.info(f'Extracting charging station nodes took {runtime:.2f} s.')

        start = perf_counter()
        del graph
        gc.collect()
        runtime = perf_counter() - start
        logger.info(f'Garbage collection took {runtime:.2f} s.')

        start = perf_counter()
        graph = read_osm(
            str(osm_path),
            charging_stations_nodes=cs_nodes,
            contract=True
        )
        runtime = perf_counter() - start
        logger.info(f'Importing map to contracted graph'
                    f' ({len(graph.nodes)} Nodes, {len(graph.edges)} Edges)'
                    f' with nodes for {len(cs_nodes)} charging stations'
                    f' took {runtime:.2f} s'
                    )
        gc.collect()

        with open(cache_path, 'wb') as f:
            pickle.dump(graph, f)
    else:
        logger.info('Rebuilding rtree..')
        graph.rebuild_rtree()

    return graph


def get_charging_stations(cs_source_fname='charging_stations.json'):
    with static_dir.joinpath(cs_source_fname).open() as f:
        charging_stations = json.load(f)
    return charging_stations


def setup_parser():
    parser = argparse.ArgumentParser(description='Run Benchmark Scripts.')
    parser.add_argument(
        'configs',
        help='List of filenames to benchmark YAML configs in ./configs.',
        type=Path,
        nargs='+'
    )

    return parser


def parse_config(path: Path) -> AnyConf:
    logger.info(f'Processing {path}..')
    with path.open() as f:
        conf = yaml.load(f, Loader=yaml.Loader)
    conf = conf_map[conf['type']](**conf)
    conf = apply_conversions(conf)
    return conf


def init_logging():
    dictConfig({
        'version': 1,
        'formatters': {
            'main': {
                'class': 'logging.Formatter',
                'format': '%(asctime)s %(name)s %(levelname)s %(message)s',
                'datefmt': '%H:%M:%S',
            }
        },
        'handlers': {
            'console': {
                'class': 'logging.StreamHandler',
                'formatter': 'main'
            }
        },
        'loggers': {
            'evaluation.lib.benchmarks': {
                'level': 'INFO',
                'handlers': ['console'],
                'propagate': 0
            },
            'evrouting.osm.imports': {
                'level': 'DEBUG',
                'handlers': ['console'],
                'propagate': 0
            },
            __name__: {
                'level': 'INFO',
                'handlers': ['console'],
                'propagate': 0
            },
        },
    })


def main():
    init_logging()

    r = base.joinpath('results')
    r.mkdir(exist_ok=True)
    r = r.joinpath(datetime.datetime.now().isoformat())
    r.mkdir()

    args = setup_parser().parse_args()
    for path in args.configs:
        benchmark_dir = r.joinpath(path.with_suffix(''))
        benchmark_dir.mkdir(exist_ok=True)

        conf = parse_config(base.joinpath('configs', path.with_suffix('.yaml')))

        charging_stations = get_charging_stations(conf.paths.charging_stations)
        graph = get_map(conf.paths.map, charging_stations)

        benchmarks_map[conf.type](G=graph,
                                  charging_stations=charging_stations,
                                  conf=conf,
                                  result_dir=benchmark_dir
                                  )


if __name__ == '__main__':
    main()
