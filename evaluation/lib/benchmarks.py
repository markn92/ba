import logging
import random
from typing import TextIO, Union, Type
from time import perf_counter
from pathlib import Path
from dataclasses import asdict, fields

import networkx as nx
from networkx.algorithms.shortest_paths.weighted import _weight_function
from evrouting.graph_tools import DISTANCE_KEY
from evaluation.lib.algorithm import _dijkstra_multisource
from evaluation.lib import queries
from evaluation.lib.config import RankConf, QueryConf, InitConf, AnyConf, QueryCycle

logger = logging.getLogger(__name__)

conf_algorithms = {
    'classic': queries.classic_query,
    'astar': queries.astar_query,
    'bidirectional': queries.bidirectional_query,
    'charge': queries.charge_query,
    'charge_tweaked': queries.tweak_charge_query,
    'gasstation': queries.gasstation_query,
    'insert': queries.insert_nodes_into_state_graph
}

conf_return_types = {
    'classic': queries.ClassicQueryRow,
    'astar': queries.AStarQueryRow,
    'bidirectional': queries.QueryRow,
    'gasstation': queries.GasstationQueryRow,
    'charge': queries.ChargeQueryRow,
    'charge_tweaked': queries.ChargeQueryRow,
    'init': queries.InitQueryRow,
    'insert': queries.InsertQueryRow
}

QueryRow = Union[
    queries.InsertQueryRow,
    queries.InitQueryRow,
    queries.ClassicQueryRow,
    queries.ChargeQueryRow,
    queries.GasstationQueryRow,
    queries.AStarQueryRow,
    queries.QueryRow
]

SEP = ','


def write_head(f: TextIO, row_class: Type[QueryRow]):
    head = SEP.join([field.name for field in fields(row_class)])
    f.write(head + '\n')


def write_row(f: TextIO, row: QueryRow):
    f.write(SEP.join([str(i) for i in asdict(row).values()]) + "\n")


def fname(algorithm_name: str) -> str:
    return f'{algorithm_name}.csv'


def _insert_charging_stations(G, charging_stations, number=None):
    start = perf_counter()
    G.insert_charging_stations(charging_stations, number)
    runtime = perf_counter() - start

    logger.info('Importing {} Charging Stations took {:.2f} s'.format(
        len(G.charging_stations),
        runtime
    ))


def _init_result_files(result_dir, conf: AnyConf):
    if type(conf) == InitConf:
        algorithms = ['init', 'insert'] + conf.algorithms
    elif type(conf) in [RankConf, QueryConf, QueryCycle]:
        algorithms = conf.algorithms
    else:
        raise ValueError(f'Unknown Config Type: {type(conf)}')

    files = [(alg, conf_return_types[alg], fname(alg)) for alg in algorithms]

    # Remove existing results and write heads
    paths = {}
    for name, row_class, filename in files:
        path = result_dir.joinpath(filename)
        paths[name] = path
        with path.open('w') as f:
            write_head(f, row_class)

    return paths


def _run_queries(func, start_nodes, target_nodes, file: TextIO, cleanup_cache=True, **kwargs):
    logger.info(f'Running {len(start_nodes)} times {func.__name__}..')
    num_total = len(start_nodes)
    for i, (s, t) in enumerate(zip(start_nodes, target_nodes)):
        logger.info(f'{i + 1}/{num_total}')
        result_data = func(s=s, t=t, **kwargs)
        write_row(file, result_data)

    # Delete cached graphs
    if cleanup_cache:
        queries.empty_cache()

    logger.info(f'Queries completed.')


def _get_target_with_rank(graph, start_node, rank):
    weight = _weight_function(graph, DISTANCE_KEY)
    return _dijkstra_multisource(
        graph,
        [start_node],
        weight=weight,
        rank=rank
    )


def _get_ranked_tasks(G, rank, number):
    """
    Generate <number> start and target nodes with Dijkstra Rank of <rank>.

    This is done by randomly sampling start nodes and finding for each of
    them a target nodes with the according rank by executing a modified
    Dijkstra routine.

    """
    target_nodes = []
    start_nodes = []
    attempts = 0
    # Try three times to find a tasks with the required rank.
    # If not enough nodes can be found, return what you have.
    while len(target_nodes) < number and attempts < 3:
        attempts += 1
        for s in random.sample(list(G.nodes), number):
            try:
                target_nodes.append(_get_target_with_rank(G, s, rank))
            except nx.NetworkXNoPath:
                continue
            start_nodes.append(s)
            if len(target_nodes) == number:
                break

    return start_nodes, target_nodes


def query_cycles(G, charging_stations, conf: QueryCycle, result_dir):
    conf.algorithms = ['charge' for i in range(len(conf.cycle_depths))]
    extra_kwargs = [{'tweak_on': True, 'tweak_depth': i}
                    for i in conf.cycle_depths]

    return query(G, charging_stations, conf, result_dir, extra_kwargs)


def query(G, charging_stations, conf: QueryConf, result_dir, extra_kwargs=None):
    paths = _init_result_files(result_dir, conf)
    if conf.charging_stations is None:
        conf.charging_stations = [None]  # = all

    if extra_kwargs is None:
        extra_kwargs = [{} for i in range(len(conf.algorithms))]

    for n_cs in conf.charging_stations:
        # Random start and target nodes
        nodes = random.sample(list(G.nodes), k=2 * conf.queries_per_row)
        start_nodes = nodes[:int(len(nodes) / 2)]
        target_nodes = nodes[int(len(nodes) / 2):]

        # Random adding of n_cs charging stations
        _insert_charging_stations(G, charging_stations, number=n_cs)

        for alg, kwargs in zip(conf.algorithms, extra_kwargs):
            func = conf_algorithms[alg]
            with paths[alg].open('a') as file:
                _run_queries(func,
                             start_nodes,
                             target_nodes,
                             file,
                             G=G,
                             conf=conf,
                             cleanup_cache=False,
                             **kwargs)

        queries.empty_cache()


def rank(G, charging_stations, conf: RankConf, result_dir: Path):
    paths = _init_result_files(result_dir, conf)
    _insert_charging_stations(G, charging_stations)  # Add all charging stations

    for r in conf.ranks:
        logger.info(f'Getting {conf.queries_per_rank} targets with rank {r}.')
        start = perf_counter()
        start_nodes, target_nodes = _get_ranked_tasks(G, r, conf.queries_per_rank)
        end = perf_counter() - start
        logger.info(f'Ranked nodes generated in {end:.2f} s')

        for alg in conf.algorithms:
            func = conf_algorithms[alg]
            with paths[alg].open('a') as file:
                _run_queries(func,
                             start_nodes,
                             target_nodes,
                             file=file,
                             G=G,
                             conf=conf,
                             cleanup_cache=False
                             )
    queries.empty_cache()


def init(G, charging_stations, conf: InitConf, result_dir: Path):
    paths = _init_result_files(result_dir, conf)

    for n_cs in conf.charging_stations:
        # Random adding of charging stations
        logger.info(f'Making {conf.inits_per_cs} initializations for {n_cs} '
                    f'Charging Stations.')
        for i in range(conf.inits_per_cs):
            logger.info(f'Init {i + 1}/{conf.inits_per_cs}')
            _insert_charging_stations(G, charging_stations, n_cs)
            result_data = queries.init_gasstation_queries(G, conf, run_id=i)

            with paths['init'].open('a') as file:
                write_row(file, result_data)

            # Nodes for insertion
            nodes = random.sample(list(G.nodes), k=2 * conf.insertions_per_cs)
            start_nodes = nodes[:int(len(nodes) / 2)]
            target_nodes = nodes[int(len(nodes) / 2):]

            with paths['insert'].open('a') as file:
                # Make insertion
                _run_queries(
                    queries.insert_nodes_into_state_graph,
                    start_nodes,
                    target_nodes,
                    file,
                    G=G,
                    conf=conf,
                    run_id=i
                )

            for alg in conf.algorithms:
                func = conf_algorithms[alg]
                with paths[alg].open('a') as file:
                    # Make insertion
                    _run_queries(func,
                                 start_nodes,
                                 target_nodes,
                                 file=file,
                                 G=G,
                                 conf=conf
                                 )
