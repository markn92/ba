import logging
import gc
from typing import Union
from dataclasses import dataclass

from time import perf_counter

import networkx as nx

from evrouting import gasstation, charge
from evrouting.charge.T import SoCFunction
from evrouting.graph_tools import (
    consumption_function_distance_factory,
    consumption_function_time_factory,
    DISTANCE_KEY,
    sum_weights
)
from evrouting.osm.profiles import car
from evrouting.osm.routing import (
    GasstationAccessFunctions,
    a_start_heuristic,
    get_coordinates
)

from evaluation.lib.algorithm import ranked_dijkstra
from evaluation.lib.config import InitConf, RankConf, AnyConf


@dataclass
class InitQueryRow:
    time_contracted_graph: float
    time_state_graph: float
    nodes_state_graph: int
    edges_state_graph: int
    nodes_contracted_graph: int
    edges_contracted_graph: int
    av_cs_dist: float
    charging_stations: int
    run_id: int


@dataclass
class InsertQueryRow:
    start_node: str
    target_node: str
    charging_stations: int
    time_insert_start: float
    time_insert_target: float
    time_contracted_graph: float
    time_state_graph: float
    run_id: int


@dataclass
class QueryRow:
    start_node: str
    target_node: str
    query_time: float
    trip_time: float
    nodes: int
    edges: int
    charging_stations: int


@dataclass
class ChargingStopsQuery(QueryRow):
    charging_stops: int
    charging_time: float


@dataclass
class GasstationQueryRow(ChargingStopsQuery):
    time_contracted_graph: float
    time_state_graph: float
    nodes_state_graph: int
    edges_state_graph: int


@dataclass
class ChargeQueryRow(ChargingStopsQuery):
    dominance_checks: int
    tweak_on: str
    tweak_depth: int


@dataclass
class ClassicQueryRow(QueryRow):
    dijkstra_rank: int


@dataclass
class AStarQueryRow(QueryRow):
    pass


CACHE = {}


def empty_cache():
    global CACHE
    CACHE = {}


def cached(key):
    """Cache return value."""

    def decorator(func):
        def getter(*args, **kwargs):
            try:
                return CACHE[key]
            except KeyError:
                ret = func(*args, **kwargs)
                CACHE[key] = ret
                return ret

        return getter

    return decorator


def no_gc(func):
    """Run func without garbage collection."""

    def inner(*args, **kwargs):
        gcold = gc.isenabled()
        gc.disable()
        try:
            r = func(*args, **kwargs)
        finally:
            if gcold:
                gc.enable()
        return r

    inner.__name__ = func.__name__

    return inner


def _format_node(G, n):
    lon, lat = get_coordinates(G, n)
    return f"{lon}:{lat}"


_cache_contraction_graph_key = 'contraction'
_cache_state_graph_key = 'state_graph'


@cached(_cache_contraction_graph_key)
def get_contracted_graph(G, conf: AnyConf, f):
    start = perf_counter()
    contracted_graph = gasstation.routing.contract_graph(
        G=G,
        charging_stations=G.charging_stations,
        capacity=conf.capacity,
        f=f
    )
    contraction_time = perf_counter() - start
    logging.info('Contracted graph with {} stations in {:.2f} s.'.format(
        len(G.charging_stations),
        contraction_time
    ))
    return contracted_graph, contraction_time


@cached(_cache_state_graph_key)
def get_state_graph(contracted_graph, conf: AnyConf, f):
    start = perf_counter()
    state_graph = gasstation.routing.state_graph(
        contracted_graph,
        conf.capacity,
        f
    )
    state_graph_time = perf_counter() - start
    logging.info('Created State Graph in {:.2f} s.'.format(
        state_graph_time
    ))
    return state_graph, state_graph_time


@no_gc
def init_gasstation_queries(G, conf: InitConf, run_id):
    def accumulate_result(func):
        results = []

        def inner(*args, **kwargs):
            r = func(*args, **kwargs)
            results.append(r)
            return r

        return inner, results

    f = GasstationAccessFunctions(conf.consumption.consumption_coefficient)
    f.shortest_path, shortest_paths = accumulate_result(f.shortest_path)

    contracted_graph, contraction_time = get_contracted_graph(G, conf, f)
    state_graph, state_graph_time = get_state_graph(contracted_graph, conf, f)

    # calc average cs distance
    av_cs_dist = 0
    if len(G.charging_stations) > 1:
        cs = list(G.charging_stations)
        num_distances = len(cs) * (len(cs) - 1) / 2
        av_cs_dist = sum(
            sum_weights(G, path, DISTANCE_KEY)
            for path in shortest_paths
        ) / num_distances

    return InitQueryRow(
        charging_stations=len(G.charging_stations),
        time_contracted_graph=contraction_time,
        time_state_graph=state_graph_time,
        nodes_state_graph=len(state_graph.nodes),
        edges_state_graph=len(state_graph.edges),
        nodes_contracted_graph=len(contracted_graph.nodes),
        edges_contracted_graph=len(contracted_graph.edges),
        av_cs_dist=av_cs_dist,
        run_id=run_id
    )


@no_gc
def insert_nodes_into_state_graph(G, conf: InitConf, s, t, run_id=0):
    f = GasstationAccessFunctions(conf.consumption.consumption_coefficient)
    contracted_graph, contraction_time = get_contracted_graph(G, conf, f)
    state_graph, state_graph_time = get_state_graph(contracted_graph, conf, f)

    start = perf_counter()
    gasstation.routing.insert_start_node(
        s,
        G,
        contracted_graph,
        G.charging_stations,
        state_graph,
        conf.capacity,
        conf.mu_s,
        f
    )
    time_insertion = perf_counter() - start

    start = perf_counter()
    gasstation.routing.insert_final_node(
        t,
        G,
        G.charging_stations,
        state_graph,
        conf.capacity,
        conf.mu_t,
        f
    )
    time_target = perf_counter() - start

    return InsertQueryRow(
        start_node=s,
        target_node=t,
        charging_stations=len(G.charging_stations),
        time_insert_start=time_insertion,
        time_insert_target=time_target,
        time_contracted_graph=contraction_time,
        time_state_graph=state_graph_time,
        run_id=run_id
    )


@no_gc
def gasstation_query(G, conf: Union[QueryRow, RankConf], s, t):
    f = GasstationAccessFunctions(conf.consumption.consumption_coefficient)
    contracted_graph, contraction_time = get_contracted_graph(G, conf, f)
    state_graph, state_graph_time = get_state_graph(contracted_graph, conf, f)

    start = perf_counter()
    result = gasstation.routing.shortest_path(
        G=G,
        charging_stations=G.charging_stations,
        s=s,
        t=t,
        initial_soc=conf.mu_s,
        final_soc=conf.mu_t,
        capacity=conf.capacity,
        f=f,
        extended_graph=state_graph,
        contracted_graph=contracted_graph
    )
    query_time = perf_counter() - start

    charge_stops = [t for n, t in result.charge_path if t > 0]
    return GasstationQueryRow(
        start_node=s,
        target_node=t,
        query_time=query_time,
        trip_time=result.trip_time,
        nodes=len(G.nodes),
        edges=len(G.edges),
        charging_stations=len(G.charging_stations),
        time_contracted_graph=contraction_time,
        time_state_graph=state_graph_time,
        charging_stops=len(charge_stops),
        charging_time=sum(charge_stops),
        nodes_state_graph=len(state_graph.nodes),
        edges_state_graph=len(state_graph.edges)
    )


@no_gc
def charge_query(G, conf: Union[QueryRow, RankConf], s, t, tweak_on=False, tweak_depth=1):
    coefficient = conf.consumption.consumption_coefficient
    if conf.consumption.type == 'time':
        c = consumption_function_time_factory(coefficient)
    else:
        c = consumption_function_distance_factory(coefficient)

    start = perf_counter()
    result = charge.shortest_path(
        G=G,
        charging_stations=G.charging_stations,
        s=s,
        t=t,
        initial_soc=conf.mu_s,
        final_soc=conf.mu_t,
        capacity=conf.capacity,
        c=c,
        tweak_on=tweak_on,
        tweak_depth=tweak_depth
    )
    runtime = perf_counter() - start
    dominance_checks = SoCFunction.counter_dominance_checks
    SoCFunction.counter_dominance_checks = 0

    charge_stops = [t for n, t in result.charge_path if t > 0]
    return ChargeQueryRow(
        start_node=s,
        target_node=t,
        query_time=runtime,
        trip_time=result.trip_time,
        nodes=len(G.nodes),
        edges=len(G.edges),
        charging_stations=len(G.charging_stations),
        charging_stops=len(charge_stops),
        charging_time=sum(charge_stops),
        dominance_checks=dominance_checks,
        tweak_on='on' if tweak_on else 'off',
        tweak_depth=tweak_depth
    )


def tweak_charge_query(G, conf: Union[QueryRow, RankConf], s, t):
    return charge_query(G, conf, s, t, tweak_on=True)


@no_gc
def classic_query(G, conf, s, t):
    start = perf_counter()
    try:
        result, rank = ranked_dijkstra(G, s, t, weight=DISTANCE_KEY)
    except nx.NetworkXNoPath:
        result, rank = None, None
    runtime = perf_counter() - start

    return ClassicQueryRow(
        start_node=s,
        target_node=t,
        query_time=runtime,
        trip_time=result,
        nodes=len(G.nodes),
        edges=len(G.edges),
        charging_stations=len(G.charging_stations),
        dijkstra_rank=rank
    )


@no_gc
def bidirectional_query(G, conf, s, t):
    start = perf_counter()
    try:
        result, _ = nx.algorithms.bidirectional_dijkstra(
            G, s, t, weight=DISTANCE_KEY)
    except nx.NetworkXNoPath:
        result = None
    runtime = perf_counter() - start

    return QueryRow(
        start_node=s,
        target_node=t,
        query_time=runtime,
        trip_time=result,
        nodes=len(G.nodes),
        edges=len(G.edges),
        charging_stations=len(G.charging_stations),
    )


@no_gc
def astar_query(G, conf, s, t):
    start = perf_counter()
    try:
        result = nx.astar_path_length(
            G,
            s, t,
            weight=DISTANCE_KEY,
            heuristic=a_start_heuristic(G, car)
        )
    except nx.NetworkXNoPath:
        result = None
    runtime = perf_counter() - start

    return AStarQueryRow(
        start_node=s,
        target_node=t,
        query_time=runtime,
        trip_time=result,
        nodes=len(G.nodes),
        edges=len(G.edges),
        charging_stations=len(G.charging_stations)
    )
