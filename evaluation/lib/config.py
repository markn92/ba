from typing import List, Union
from dataclasses import dataclass, field


@dataclass
class PathsConf:
    charging_stations: str
    map: str


@dataclass
class ConsumptionConf:
    consumption_coefficient: float
    type: str


@dataclass
class BaseConf:
    type: str
    description: str
    paths: PathsConf
    mu_s: float
    mu_t: float
    capacity: float
    consumption: ConsumptionConf

    def __post_init__(self):
        self.paths = PathsConf(**self.paths)
        self.consumption = ConsumptionConf(**self.consumption)


@dataclass
class RankConf(BaseConf):
    queries_per_rank: int
    ranks: List[int]
    algorithms: List[str]


@dataclass
class QueryConf(BaseConf):
    queries_per_row: int
    charging_stations: List[int] = None
    algorithms: List[str] = field(
        default_factory=[
            'classic',
            'astar',
            'bidirectional',
            'gasstation',
            'charge'
        ])


@dataclass
class QueryCycle(BaseConf):
    queries_per_row: int
    cycle_depths: List[int]
    charging_stations: List[int] = None


@dataclass
class InitConf(BaseConf):
    insertions_per_cs: int
    inits_per_cs: int
    charging_stations: List[int]
    algorithms: List[str] = field(default_factory=[])


def apply_conversions(conf: BaseConf):
    """kWh to Wh"""
    conf.capacity *= 1000
    if conf.consumption.type == 'time':
        conf.consumption.consumption_coefficient *= 1000
    conf.mu_s *= 1000
    conf.mu_t *= 1000

    return conf


AnyConf = Union[RankConf, QueryConf, QueryCycle, InitConf]
