"""A setuptools based setup module.
See:
https://packaging.python.org/guides/distributing-packages-using-setuptools/
https://github.com/pypa/sampleproject
"""

# Always prefer setuptools over distutils
from setuptools import setup, find_packages
from os import path

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='evrouting',
    version='0.0.1',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://git.imp.fu-berlin.de/markn92/ba',
    author='Mark Niehues',
    classifiers=[
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
    packages=find_packages(),
    python_requires='>=3.4.*, <4',
    install_requires=[
        'networkx',
        'requests',
        'rtree'
    ]
)
